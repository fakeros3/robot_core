use crate::base::InvokeRequest;
use crate::node::node_client::NodeClient;
use futures::future;
use ringbuffer::{AllocRingBuffer, RingBufferExt, RingBufferWrite};
use std::collections::hash_map::Entry;
use std::collections::{HashMap, HashSet};
use std::sync::{Arc, RwLock};
use tokio::sync::broadcast;
use tokio::time::{self, Duration, Instant};

pub struct Context {
    pub shared: Arc<RwLock<State>>,
}

pub struct State {
    pub nodes: HashMap<String, NodeInfo>,
    pub topics: HashMap<String, TopicInfo>,
}

pub struct NodeInfo {
    pub name: String,
    pub endpoint: String,
    pub methods: HashSet<String>,
    pub check_health_failed_times: usize,
}

pub struct TopicInfo {
    pub name: String,

    pub tx: broadcast::Sender<String>,
    pub pubers: HashSet<String>,
    pub subers: HashSet<String>,
    pub history: AllocRingBuffer<String>,
    pub last_pub_time: Instant,
}

impl Context {
    pub fn new() -> Context {
        let state = Arc::new(RwLock::new(State {
            nodes: HashMap::new(),
            topics: HashMap::new(),
        }));

        // spawn a background task
        // 1. check node health
        tokio::spawn(some_background_task(state.clone()));

        Context { shared: state }
    }

    pub fn get_svc_address(&self, name: &str) -> Option<String> {
        let state = self.shared.read().unwrap();
        state.nodes.get(name).map(|v| v.endpoint.clone())
    }

    pub fn check_svc(&self, name: &str) -> bool {
        let state = self.shared.read().unwrap();
        state.nodes.contains_key(name)
    }

    pub fn registe_svc(&self, name: &str, address: &str) -> bool {
        self.unregiste_svc(name);
        let mut state = self.shared.write().unwrap();

        match state.nodes.entry(name.to_string()) {
            Entry::Occupied(_e) => false,
            Entry::Vacant(e) => {
                e.insert(NodeInfo {
                    name: name.to_string(),
                    endpoint: address.to_string(),
                    methods: HashSet::new(),
                    check_health_failed_times: 0,
                });
                true
            }
        }
    }

    pub fn unregiste_svc(&self, name: &str) -> bool {
        let mut state = self.shared.write().unwrap();
        if let Some(_node) = state.nodes.remove(name) {
            // TODO: remove node from TopicInfo
            true
        } else {
            false
        }
    }

    pub fn registe_method(&self, name: &str, method: &str) {
        let mut state = self.shared.write().unwrap();

        state
            .nodes
            .get_mut(name)
            .map(|v| v.methods.insert(method.to_string()));
    }

    pub fn publish(&self, topic: &str, value: &str) -> usize {
        let mut state = self.shared.write().unwrap();

        let t = state.get_topic_mut(topic);
        t.history.push(value.to_string());
        t.last_pub_time = Instant::now();
        t.tx.send(value.to_string()).unwrap_or(0)
    }

    pub fn get_history_msg(&self, topic: &str, count: isize) -> Option<Vec<String>> {
        let state = self.shared.read().unwrap();

        if let Some(t) = state.topics.get(topic) {
            let r = (-count..0)
                .map(|i| t.history.get(i))
                .filter_map(|e| e.cloned())
                .collect::<Vec<String>>();
            Some(r)
        } else {
            None
        }
    }

    pub fn subscribe(&self, topic: &str) -> broadcast::Receiver<String> {
        let mut state = self.shared.write().unwrap();

        let t = state.get_topic_mut(topic);
        t.tx.subscribe()
    }

    pub fn add_suber(&self, topic: &str, node: &str) {
        let mut state = self.shared.write().unwrap();

        let t = state.get_topic_mut(topic);
        t.subers.insert(node.to_string());
    }

    pub fn remove_suber(&self, topic: &str, node: &str) {
        let mut state = self.shared.write().unwrap();

        state.topics.get_mut(topic).map(|v| v.subers.remove(node));
    }

    pub fn add_puber(&self, topic: &str, node: &str) {
        let mut state = self.shared.write().unwrap();

        let t = state.get_topic_mut(topic);
        t.pubers.insert(node.to_string());
    }
}

impl TopicInfo {
    fn new(topic: &str) -> TopicInfo {
        let (tx, _) = broadcast::channel(1024);
        TopicInfo {
            name: topic.to_string(),
            tx,
            pubers: HashSet::new(),
            subers: HashSet::new(),
            history: AllocRingBuffer::with_capacity(64),
            last_pub_time: Instant::now(),
        }
    }
}

impl State {
    fn get_topic_mut(&mut self, topic: &str) -> &mut TopicInfo {
        self.topics
            .entry(topic.to_string())
            .or_insert(TopicInfo::new(topic))
    }

    fn check_node(&mut self, name: &str, ok: bool) {
        if let Some(n) = self.nodes.get_mut(name) {
            if ok {
                n.check_health_failed_times = 0;
            } else {
                n.check_health_failed_times += 5;
            }
        }
    }
}

async fn check_health(node: String, addr: String) -> bool {
    let addr = format!("http://{}", &addr);
    if let Ok(mut cli) = NodeClient::connect(addr).await {
        let req = InvokeRequest {
            service: node,
            method: "__health".to_string(),
            msg: "{}".to_string(),
        };
        let req = tonic::Request::new(req);
        // req.set_timeout()
        if let Ok(rep) = cli.invoke(req).await {
            return rep.get_ref().code == 0;
        }
    }

    false
}

async fn some_background_task(shared: Arc<RwLock<State>>) {
    let mut interval = time::interval(Duration::from_millis(1000));
    loop {
        interval.tick().await;

        // check node health
        let mut nodes = Vec::new();
        {
            let state = shared.read().unwrap();
            for (k, v) in &state.nodes {
                if !v.endpoint.is_empty() {
                    nodes.push((k.clone(), v.endpoint.clone()))
                }
            }
        }

        let tasks = nodes
            .iter()
            .map(|(k, v)| {
                let name = k.clone();
                let addr = v.clone();
                tokio::spawn(async move {
                    let ok = check_health(name.clone(), addr).await;
                    (name, ok)
                })
            })
            .collect::<Vec<_>>();

        let results = future::join_all(tasks).await;
        let mut state = shared.write().unwrap();
        for r in results {
            if let Ok((name, ok)) = r {
                state.check_node(&name, ok);
            }
        }
    }
}
