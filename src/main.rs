pub mod ctx;
mod pb;
pub mod rate;

use node::node_client::NodeClient;
use pb::*;
use robot_core::robot_core_server::{RobotCore, RobotCoreServer};
use std::iter::Iterator;
use std::pin::Pin;
use tokio_stream::{wrappers::BroadcastStream, Stream, StreamExt};
use tonic::{Request, Response, Status};

const NODE_NAME: &str = "robot_core";
const _NODE_META_KEY: &str = "__node";
const _SPECIAL_NODE: &str = "__istools";

struct MyRobotCore {
    ctx: ctx::Context,
}

impl MyRobotCore {}

#[tonic::async_trait]
impl RobotCore for MyRobotCore {
    async fn invoke(
        &self,
        request: Request<base::InvokeRequest>,
    ) -> Result<Response<base::InvokeReply>, Status> {
        let req = request.into_inner();
        let rep = base::InvokeReply::new();
        if req.service == NODE_NAME {
            return Ok(Response::new(rep));
        }

        if let Some(addr) = self.ctx.get_svc_address(&req.service) {
            let addr = format!("http://{}", addr);
            if let Ok(mut cli) = NodeClient::connect(addr.clone()).await {
                cli.invoke(Request::new(req)).await
            } else {
                Err(Status::unavailable(format!(
                    "Can't connect to {}({})",
                    req.service, addr
                )))
            }
        } else {
            Err(Status::not_found(format!(
                "Node({}) not found",
                req.service
            )))
        }
    }

    async fn register_service(
        &self,
        request: Request<robot_core::ServiceRequest>,
    ) -> Result<Response<robot_core::ServiceResponse>, Status> {
        let req = request.get_ref();
        let registed = self
            .ctx
            .registe_svc(req.name.as_str(), req.endpoint.as_str());
        Ok(Response::new(robot_core::ServiceResponse { ok: registed }))
    }

    async fn unregister_service(
        &self,
        _request: Request<robot_core::ServiceRequest>,
    ) -> Result<Response<robot_core::ServiceResponse>, Status> {
        Err(Status::not_found(""))
    }

    async fn check_service_health(
        &self,
        _request: Request<robot_core::ServiceRequest>,
    ) -> Result<Response<robot_core::ServiceResponse>, Status> {
        Err(Status::not_found(""))
    }

    async fn list_service(
        &self,
        _request: Request<google::protobuf::Empty>,
    ) -> Result<Response<robot_core::ListServiceResponse>, Status> {
        let state = self.ctx.shared.read().unwrap();
        let vec = state
            .nodes
            .iter()
            .map(|(k, v)| robot_core::ServiceInfo {
                name: k.to_string(),
                failed: v.check_health_failed_times as i32,
            })
            .collect::<Vec<robot_core::ServiceInfo>>();
        let rep = robot_core::ListServiceResponse { datas: vec };
        Ok(Response::new(rep))
    }

    async fn register_service_method(
        &self,
        request: Request<robot_core::ServiceMethod>,
    ) -> Result<Response<google::protobuf::Empty>, Status> {
        let req = request.get_ref();
        self.ctx.registe_method(&req.service, &req.method);
        Ok(Response::new(google::protobuf::Empty {}))
    }

    async fn list_method(
        &self,
        _request: Request<google::protobuf::Empty>,
    ) -> Result<Response<robot_core::ServiceMethodArray>, Status> {
        let state = self.ctx.shared.read().unwrap();
        let vec = state
            .nodes
            .iter()
            .flat_map(|(k, v)| v.methods.iter().map(move |m| (k, m)))
            .map(|(n, m)| robot_core::ServiceMethod {
                service: n.to_string(),
                method: m.to_string(),
            })
            .collect::<Vec<robot_core::ServiceMethod>>();
        let rep = robot_core::ServiceMethodArray { methods: vec };
        Ok(Response::new(rep))
    }

    async fn publish(
        &self,
        request: Request<robot_core::PublishRequest>,
    ) -> Result<Response<google::protobuf::Empty>, Status> {
        let req = request.get_ref();
        let metadata = request.metadata();
        let node = metadata.get(_NODE_META_KEY);
        let node = node.expect("__unknown").to_str().unwrap();
        let istools = metadata.get(_SPECIAL_NODE).is_some();

        if !istools && !self.ctx.check_svc(&node) {
            return Err(Status::data_loss("unknown node."));
        }

        if !istools {
            self.ctx.add_puber(&req.topic, &node);
        }
        self.ctx.publish(&req.topic, &req.msg);
        Ok(Response::new(google::protobuf::Empty {}))
    }

    async fn multi_publish(
        &self,
        request: Request<robot_core::MultiPublishRequest>,
    ) -> Result<Response<google::protobuf::Empty>, Status> {
        let req = request.get_ref();
        let metadata = request.metadata();
        let node = metadata.get(_NODE_META_KEY);
        let node = node.expect("__unknown").to_str().unwrap();
        let istools = metadata.get(_SPECIAL_NODE).is_some();

        if !istools && !self.ctx.check_svc(&node) {
            return Err(Status::data_loss("unknown node."));
        }

        for t in &req.topics {
            if !istools {
                self.ctx.add_puber(&t.topic, &node);
            }

            self.ctx.publish(&t.topic, &t.msg);
        }
        Ok(Response::new(google::protobuf::Empty {}))
    }

    type SubscribeStream = Pin<
        Box<
            dyn Stream<Item = Result<robot_core::SubscribeResponse, Status>>
                + Send
                + Sync
                + 'static,
        >,
    >;

    async fn subscribe(
        &self,
        request: Request<robot_core::SubscribeRequest>,
    ) -> Result<Response<Self::SubscribeStream>, Status> {
        let req = request.get_ref();
        let metadata = request.metadata();
        let node = metadata.get(_NODE_META_KEY);
        let node = node.expect("__unknown").to_str().unwrap();
        let istools = metadata.get(_SPECIAL_NODE).is_some();

        if !istools && !self.ctx.check_svc(&node) {
            return Err(Status::data_loss("unknown node."));
        }

        if !istools {
            self.ctx.add_suber(&req.topic, &node);
        }
        let rx = self.ctx.subscribe(&req.topic);
        let s = BroadcastStream::new(rx)
            .filter_map(|item| {
                if let Some(i) = item.ok() {
                    Some(robot_core::SubscribeResponse { msg: i })
                } else {
                    None
                }
            })
            .map(Ok);

        if req.latch {
            if let Some(history) = self.ctx.get_history_msg(&req.topic, req.count as isize) {
                let x = history
                    .into_iter()
                    .map(|e| Ok(robot_core::SubscribeResponse { msg: e.clone() }))
                    .collect::<Vec<Result<robot_core::SubscribeResponse, Status>>>();
                let s = tokio_stream::iter(x).chain(s);
                return Ok(Response::new(Box::pin(s)));
            }
        }

        let s: Self::SubscribeStream = Box::pin(s);
        Ok(Response::new(s))
    }

    async fn unsubscribe(
        &self,
        _request: Request<robot_core::SubscribeRequest>,
    ) -> Result<Response<google::protobuf::Empty>, Status> {
        Err(Status::not_found(""))
    }

    async fn list_topic(
        &self,
        request: Request<robot_core::ListTopicRequest>,
    ) -> Result<Response<robot_core::ListTopicReply>, Status> {
        let state = self.ctx.shared.read().unwrap();
        let req = request.get_ref();
        let vec = match &req.topic {
            Some(t) => {
                let r = state.topics.get(t).map(|v| robot_core::TopicDetail {
                    topic: v.name.clone(),
                    msg: "".to_string(),
                    pubers: v.pubers.iter().map(|x| x.clone()).collect::<Vec<String>>(),
                    subers: v.subers.iter().map(|x| x.clone()).collect::<Vec<String>>(),
                });
                let mut vec = Vec::new();
                if let Some(rr) = r {
                    vec.push(rr);
                }
                vec
            }
            None => state
                .topics
                .iter()
                .map(|(k, v)| robot_core::TopicDetail {
                    topic: k.to_string(),
                    msg: "".to_string(),
                    pubers: v.pubers.iter().map(|x| x.clone()).collect::<Vec<String>>(),
                    subers: v.subers.iter().map(|x| x.clone()).collect::<Vec<String>>(),
                })
                .collect::<Vec<robot_core::TopicDetail>>(),
        };

        Ok(Response::new(robot_core::ListTopicReply { topics: vec }))
    }
}

async fn start_grpc_server() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "0.0.0.0:11311".parse().unwrap();
    println!("robot_core listening on: {}", addr);

    let svc = RobotCoreServer::new(MyRobotCore {
        ctx: ctx::Context::new(),
    });

    tonic::transport::Server::builder()
        .add_service(svc)
        .serve(addr)
        .await?;
    Ok(())
}

// tonic in single thread may has 2x speed.
//    refer: https://github.com/hyperium/tonic/issues/784
// tonic may 3xslower than grpc-rs(grpc-c++)
//    refer: https://github.com/hyperium/tonic/issues/211
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let rate = std::sync::Arc::new(std::sync::Mutex::new(rate::Rate::new(52)));
    let rt = tokio::runtime::Builder::new_current_thread()
        .on_thread_park(move || {
            let mut r = rate.lock().unwrap();
            r.sleep();
        })
        .enable_all()
        .build()?;
    rt.block_on(start_grpc_server())
}

// #[tokio::main]
// async fn main() -> Result<(), Box<dyn std::error::Error>> {
//     start_grpc_server().await
// }
