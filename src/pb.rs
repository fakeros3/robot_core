pub mod base {
    tonic::include_proto!("base");

    impl InvokeReply {
        pub fn new() -> InvokeReply {
            InvokeReply {
                code: 0,
                msg: "".to_string(),
                data: "{}".to_string(),
            }
        }
    }
}

pub mod node {
    tonic::include_proto!("node");
}

pub mod robot_core {
    tonic::include_proto!("robot_core");
}

pub mod google {
    pub mod protobuf {
        tonic::include_proto!("google.protobuf");
    }
}

