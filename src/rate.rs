use tokio::time::Instant;

pub struct Rate {
    duration: i32,
    start: Instant,
}

impl Rate {
    pub fn new(hz: i32) -> Self {
        Rate {
            duration: 1000 / hz,
            start: Instant::now(),
        }
    }

    pub fn sleep(&mut self) {
        let cost = (Instant::now() - self.start).as_millis();
        let left = self.duration - cost as i32;
        if left > 0 {
            std::thread::sleep(std::time::Duration::from_millis(left as u64));
        }
        self.start = Instant::now();
    }
}
